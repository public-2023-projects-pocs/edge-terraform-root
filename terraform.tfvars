##################################################################################
# Spectro Cloud credentials
##################################################################################

sc_host         = "api.spectrocloud.com" #e.g: api.spectrocloud.com (for SaaS)
sc_api_key      = ""
sc_project_uid  = ""
sc_api_endpoint = "https://api.spectrocloud.com/v1/edgehosts"