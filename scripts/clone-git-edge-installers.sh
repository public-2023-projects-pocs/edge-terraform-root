#!/bin/bash
# Clone-git-edge-installers.sh

set -eo pipefail

FOLDER_GIT="${1:-/home/ubuntu}"
USERNAME=$(cat "$(dirname $0)/.creds"   | grep username | awk -F "username=" '{print $2}')
PASSWORD=$(cat "$(dirname $0)/.creds" | grep password | awk -F "password=" '{print $2}')
CLONE_REPO_GIT="${2:-https://$USERNAME:$PASSWORD@bitbucket.dentsplysirona.com/scm/lig/edge-installers.git}"

if [[ -d "${FOLDER_GIT}/edge-installers" ]]; then
  git -C "${FOLDER_GIT}/edge-installers" pull
else
  git clone ${CLONE_REPO_GIT} ${FOLDER_GIT}
fi

set +eo pipefail
EXIT_CODE=0

exit $EXIT_CODE