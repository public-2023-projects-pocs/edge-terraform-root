##################################################################################
# Spectro Cloud credentials
##################################################################################

variable "sc_host" {
  type = string
}

variable "sc_api_key" {
  type = string
}

variable "sc_project_uid" {
  type = string
}

variable "sc_api_endpoint" {
  type = string
}