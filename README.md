# Terraform Provider for Ansible

The Terraform Provider for Ansible provides a more straightforward and robust means of executing Ansible automation from Terraform than local-exec. Paired with the inventory plugin in [the Ansible cloud.terraform collection](https://github.com/ansible-collections/cloud.terraform), users can run Ansible playbooks and roles on infrastructure provisioned by Terraform. The provider also includes integrated ansible-vault support. 

This provider can be [found in the Terraform Registry here](https://registry.terraform.io/providers/ansible/ansible/latest).

For more details on using Terraform and Ansible together see these blog posts:

* [Terraforming clouds with Ansible](https://www.ansible.com/blog/terraforming-clouds-with-ansible)
* [Walking on Clouds with Ansible](https://www.ansible.com/blog/walking-on-clouds-with-ansible)
* [Providing Terraform with that Ansible Magic](https://www.ansible.com/blog/providing-terraform-with-that-ansible-magic)


## Requirements

- install Go: [official installation guide](https://go.dev/doc/install)
- install Terraform: [official installation guide](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
- install Ansible: [official installation guide](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

sudo dnf install python3-pip
python3 -m pip install --user pipx
python3 -m pipx ensurepath

on your client or on your cloud shell where you run ansible, you must first install:
ansible-galaxy collection install cloud.terraform --force
ansible-galaxy collection install community.general --force

in your provider.tf also must be ansible:

ansible = {
  #version = "~> 1.1.0"
  source  = "ansible/ansible"
than you must add to your terraform project directory the "inventory.yml" with this line in:
plugin: cloud.terraform.terraform_provider