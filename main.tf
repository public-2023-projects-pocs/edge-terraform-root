resource "ansible_playbook" "playbook" {
  playbook   = "playbook.yml"
  name       = "ubuntu-spectro-edge-creator"
  verbosity  = 0

  extra_vars = {
    ansible_connection = "local"
    sc_project_uid     = var.sc_project_uid
    sc_api_key         = var.sc_api_key
    sc_api_endpoint    = var.sc_api_endpoint
  }
}

output "ansible_playbook_stdout" {
  value = ansible_playbook.playbook.ansible_playbook_stdout
}

output "ansible_playbook_stderr" {
  value = ansible_playbook.playbook.ansible_playbook_stderr
}