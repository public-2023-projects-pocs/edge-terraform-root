terraform {
  required_providers {
    ansible = {
      version = "1.1.0"
      source  = "ansible/ansible"
    }
  }
}

provider "ansible" {
  # collections_paths = "collections/ansible_collections"
}